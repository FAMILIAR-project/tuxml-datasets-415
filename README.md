# tuxml-datasets-415

**!! I've moved the final `pkl`dataset here: https://gitlab.com/FAMILIAR-project/tuxml-size-analysis-datasets because of issues with git lfs !!**

(Large) data for the analysis of results (replication with version 4.15)
The database is a bit different also. 

This repo. contains the script for extracting/conversion and datasets
`config415_bdd30000-40000.csv` is a CSV file with cid=30000 to cid=40000 (so ~10K entries) extracted from the database

note: we could merge all together in https://github.com/TuxML/tuxml-datasets but it's preferable at this step to separate them
